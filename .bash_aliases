## source ~/.bashrc
#alias ls='ls --color=auto'
alias mutt-spam='mutt -f ~/.mail/spam'
alias myip='curl ipinfo.io/ip'
alias grepve="grep -v -e '#' -e '^$'"
alias tmuxvte='tmux a -t vtel'
alias sshec='ssh -i ~/.ssh/ec2-user.id_rsa'
alias kubectl-dev01='kubectl --kubeconfig ~/.kube/dev01-config'