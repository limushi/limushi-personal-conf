This is plain project with personal configs for Windows, Linux and tips to save/update them.


#~/.nanorc

#~/.vimrc

#~/.muttrc

#~/bin - a lot of scripts

Help with MD: https://markdown.land/markdown-code-block

Example install:
```bash
git clone https://gitlab.com/limushi/limushi-personal-conf.git
cp limushi-personal-conf/.nanorc ~/
cp limushi-personal-conf/.vimrc ~/
```
